.. include:: ../README.rst

Topics
======

.. _documentation-topics:

.. toctree::
   tutorial
   performance
   reference
