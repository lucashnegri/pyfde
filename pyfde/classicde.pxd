
from rng cimport Random
cdef double clip(double min_, double max_, double val)

cdef class ClassicDE:
    """
    Implements the classic differential evolution algorithm (DE/rand/1/bin).
    Holds the vector population, fitness evaluations, and the parameters.

    Parameters
    ----------
    fitness : function
        Fitness evaluation function to be used. Must accept a double array
        as parameter and return the fitness evaluation as a float.

        Alternatively (by using batch=True), it can be a function that accepts
        two parameters: a double[n_pop, n_dim] corresponding to the population
        to be evaluated and a double[n_pop], correponding to where the fitness
        evaluations must be written to.
    n_dim : int, > 0
        Number of dimensions
    n_pop : int, > 3
        Number of vectors in the population
    limits : tuple (min, max), list of tuples or double[n_dim][2]
        Limits for the search space dimension. If it is a tuple, then every
        dimension will have the same limits. If it is a list of tuples or an
        array, then each dimension will have the corresponding limit.
    batch : bool
        If the `batch` mode should be used.
    minimize : bool
        If the fitness function should be minimized. Default is 
        minimize=False.
    seed : int, > 0
        Seed used to initialize the pseudo random number generator.
    """

    cdef public double f
    """Differential weight, in the [0., 2.] range"""

    cdef public double cr
    """Crossover rate, in the [0., 1.] range"""

    cdef pop, trial, fit, trial_fit, fitness, limits
    cdef bint batch, minimize

    cdef k_high
    """Cache of the indexes with the highest fitness"""

    cdef Random rng
