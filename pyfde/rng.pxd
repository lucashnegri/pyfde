#cython: embedsignature=True, boundscheck=False, wraparound=False, cdivision=True

cdef extern from "limits.h":
    enum: ULLONG_MAX

ctypedef unsigned long long uint64_t
cdef uint64_t RAND_MAX = ULLONG_MAX

cdef class Random:
    """A fast pseudo random number generator. Used to compute the needed
    random numbers from uniform distributions, that are needed in the
    inner loops."""
    cdef uint64_t s[2]

    cpdef rand_seed(self, uint64_t x)
    cpdef uint64_t rand(self)
    cpdef uint64_t rand_int(self, uint64_t limit)
    cpdef double rand_double(self)
    cpdef double randn(self, double mu, double sigma)
    cpdef double randc(self, double mu, double sigma)

